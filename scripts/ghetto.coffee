# Commands:
#   hubot ghetto <text> - Real talk yo
#
# Author:
#   ChauffeR

QS = require "querystring"

module.exports = (robot) ->
  robot.respond /ghetto ((?:.|\s)+)/i, (msg) ->
    text = msg.match[1]
    data = QS.stringify({'translatetext' : text})

    if text is false
      return

    msg.http("http://www.gizoogle.net/textilizer.php")
        .header('Content-Type', 'application/x-www-form-urlencoded')
        .post(data) (err, res, body) ->

          if err
            msg.send "Somethang went wrong"
            return
          regex = body.match(/<textarea .*;\"\/>((?:.|\s)+)<\/textarea>/)
          translatedtext = regex[1]

          if translatedtext is ""
            translatedtext = "Shiznit happens"

          msg.send translatedtext
